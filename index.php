<?php

// require config
require("./config/config.php");
require(ABS_DIR."./lib/handler.class.php");

if(!$_POST) { // some debug for building
	$body = (array)json_decode('{
	    "canon_url": "https://bitbucket.org",
	    "commits": [
	        {
	            "author": "marcus",
	            "branch": "master",
	            "files": [
	                {
	                    "file": "somefile.py",
	                    "type": "modified"
	                }
	            ],
	            "message": "Added some featureA things",
	            "node": "d14d26a93fd2",
	            "parents": [
	                "1b458191f31a"
	            ],
	            "raw_author": "Marcus Bertrand <marcus@somedomain.com>",
	            "raw_node": "d14d26a93fd28d3166fa81c0cd3b6f339bb95bfe",
	            "revision": 3,
	            "size": -1,
	            "timestamp": "2012-05-30 06:07:03",
	            "utctimestamp": "2012-05-30 04:07:03+00:00"
	        },
	        {
	            "author": "marcus",
	            "branch": "master",
	            "files": [
	                {
	                    "file": "somefile.py",
	                    "type": "modified"
	                }
	            ],
	            "message": "Added some featureA things",
	            "node": "d14d26a93fd2",
	            "parents": [
	                "1b458191f31a"
	            ],
	            "raw_author": "Marcus Bertrand <marcus@somedomain.com>",
	            "raw_node": "d14d26a93fd28d3166fa81c0cd3b6f339bb95bfe",
	            "revision": 3,
	            "size": -1,
	            "timestamp": "2012-05-30 06:07:03",
	            "utctimestamp": "2012-05-30 04:07:03+00:00"
	        }
	    ],
	    "repository": {
	        "absolute_url": "/adido/bhlive.co.uk/",
	        "fork": false,
	        "is_private": true,
	        "name": "Project X",
	        "owner": "marcus",
	        "scm": "hg",
	        "slug": "bhlive.co.uk",
	        "website": ""
	    },
	    "user": "marcus"
	}',true );
} else {
	$body = json_decode($_POST["payload"],true);
file_put_contents("/tmp/post",serialize($_POST));
}

// handle request
$handler = new handler($body);
