<?php
// GITURL - .git path
// add this:  -DnestedView=DeployTest-branches to example.syncREPONAME.xml
// <switches>-DjenkinsUrl=http://localhost:8080 -DjenkinsUser=mark -DjenkinsPassword=test -DgitUrl=GITURL -DtemplateJobPrefix=DeployTest -DtemplateBranchName=master</switches>
// name=JOBNAME

class jenkinsApi {
	private $extType = "/api/json";
	private $repoSlug = '';
	private $repoInfo;
	private $gitPath = "git@bitbucket.org:";

	function createNewJobGroup() {
		// loop through all example configs in jobs/ and update place holders and send
		foreach(glob("jobs/example.*.xml") as $file) {
			if(1 || $file == "jobs/example.sync-REPONAME.xml") {
				$xml = file_get_contents($file);
				$jobName = str_replace(array("REPONAME","jobs/example.",".xml"),array($this->repoSlug,""),$file);
				echo "\n".$jobName;

				$gitUrl = $this->gitPath.trim($this->repoInfo["absolute_url"],"/").".git";
				$xml = str_replace(
					array("GITURL","REPONAME",		"RELATIVEURL"							, "JOBNAME"),
					array($gitUrl, $this->repoSlug, trim($this->repoInfo["absolute_url"],"/"), $jobName),
				$xml);

				echo "\n".$gitUrl;

				$res = $this->post("/createItem?name=$jobName",$xml);
				print_r($res);
			}
		}
	}

	function runBranchJobs($branches) {
		$mainData = $this->get($this->extType);
		$allJobs = $mainData->jobs;

		echo "Branches to run";
		print_r($branches);

		foreach($allJobs as $job) {
			foreach($branches as $branch) {
				$regex = "/".$this->repoSlug."-(.*)-".$branch."/";
				$res = preg_match($regex, $job->name);
				if($res) {
					// echo $job->name." -> ".$this->repoSlug."-$branch -- $regex\n";
					$this->runJob($job->name);
				}
			}
		}
	}

	function runSyncJob() {
		$this->runJob('sync-'.$this->repoSlug);
	}

	function runJob($jobName) {
		$url = "/job/".$jobName."/build?token=".BITBUCKET_TOKEN;
		echo "Job to run: $jobName\nurl: $url\n";
		return $this->get($url);
	}


	/** check if a repo slug has a production job - returns true / false **/
	function hasProductionJob() {
		$mainData = $this->get($this->extType);
		$allJobs = $mainData->jobs;

		if(!count($allJobs)) return false; // no jobs so create a new one

		foreach($allJobs as $job) {
			$matches = preg_match("/".$this->repoSlug."-production-trigger/",$job->name);
			if($matches) {
				return true; // our production trigger exists so we do not need to create
			}
		}
		return false; // no production job - create all new jobs
	}


	function __construct($repo) {
		$this->repoInfo = $repo;
		$this->repoSlug = $repo["slug"];
	}

	/** get to jenkins **/
	private function get($url, $data=false) {
		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, JENKINS_URL.$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);

		//execute post
		$result = curl_exec($ch);

		//close connection
		curl_close($ch);

		return json_decode($result);
	}

	/** post to jenkins **/
	private function post($url, $xmlStr) {

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, JENKINS_URL.$url);
		curl_setopt($ch,CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch,CURLOPT_POSTFIELDS, $xmlStr);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		    'Content-Type: text/xml',
		    'Content-Length: ' . strlen($xmlStr))
		);
		//execute post
		$result = curl_exec($ch);

		//close connection
		curl_close($ch);

		return $result;
	}

}

