<?php
require(ABS_DIR."./lib/jenkinsApi.class.php");

class handler {
	private $jenkins;
	private $repoData;
	private $repoSlug;
	private $repoInfo;

	function __construct($data) {
		$this->repoData = $data;
		$this->repoInfo = $data["repository"];
		$this->repoSlug = $this->repoInfo["slug"];

		$this->jenkins = new jenkinsApi($this->repoInfo);

		// check to see if the repo has some jobs and handle each case
		$productionJobsExist = $this->jenkins->hasProductionJob($this->repoSlug);

		// if production job exists - check and run jobs, otherwise create new jobs
		if($productionJobsExist) {
			$this->runJobs();
		} else {
			$this->createNewJobs();
		}

	}

	function runJobs() {
		$branchesToRun = array();
		//  loop through every commit and see what branches have been worked on
		foreach($this->repoData["commits"] as $commit) {
			$branchesToRun[] = $commit["branch"];
		}
		$branchesToRun = array_unique($branchesToRun);

		$this->jenkins->runBranchJobs($branchesToRun);
		$this->jenkins->runSyncJob();
	}

	function createNewJobs() {
		echo "Create new jobs";
		$this->jenkins->createNewJobGroup();
	}
}